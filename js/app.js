// Теоретичне питання:
// Для роботи з input не рекомендується використовувати клавіатуру тому, що існують й інші
// способи вставки тексту в input, які теж треба відслідковувати.

// Завдання:

const buttons = document.querySelectorAll('.btn');
document.addEventListener('keydown', (key) => {
    buttons.forEach(button => {
        if (key.code === `Key${button.dataset.btn}` || key.code === `${button.dataset.btn}`) {
            buttons.forEach( el => {
                el.style.background = 'black';
            });
            button.style.background = 'blue';
          }
    })
  });